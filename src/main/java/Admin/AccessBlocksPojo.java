package Admin;

public class AccessBlocksPojo {
	private int id;
	private String accessBlock;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccessBlock() {
		return accessBlock;
	}

	public void setAccessBlock(String accessBlock) {
		this.accessBlock = accessBlock;
	}

}

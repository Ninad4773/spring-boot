package com.user;

import java.sql.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import Admin.PaymentPojo;

@Component
public class UserRegister {

	@Autowired
	JdbcTemplate jdbcTemplate;

//user registration
//  public String userRegister(String firstName,String lastName,int contactNo,String Email,String Password,String Distributer,String appId,String deviceId,String appKey,String regiDate,String expireDate, String Status) throws ClassNotFoundException, SQLException{
//	
//	  
//	  
//	  jdbcTemplate.update("insert into register (firstName, lastName, contactNo, Email, Password, Distributer, appId. deviceId, appKey, regiDate, expireDate, Status) values(?,?,?,?,?,?,?,?,?,?,?,?)",new Object[]{firstName,lastName,contactNo,Email,Password,Distributer,appId,deviceId,appKey,regiDate,expireDate,Status});
//		return "register success";
//	  }

	public int setNotification(userPojo userInfo) {

		jdbcTemplate.update(
				"insert into notiifications (firstName, lastName, contactNo, email, password, distributerId, appId, deviceID, notiDate, notiTime) values (?,?,?,?,?,?,?,?,?,?)",
				new Object[] { userInfo.getFirstName(), userInfo.getLastName(), userInfo.getContactNo(),
						userInfo.getEmail(), userInfo.getPassword(), userInfo.getDistributerId(), userInfo.getAppId(),
						userInfo.getDeviceId(), userInfo.getNotiDate(), userInfo.getNotiTime() });
		return 1;
	}

	public List<userPojo> getNotificationDetails() {
		List<userPojo> getUserInfo = jdbcTemplate.query(
				"select firstName, lastName, contactNo, email, password, distributerId, appId, deviceID, notiDate, notiTime from notiifications order by id desc",
				new BeanPropertyRowMapper(userPojo.class));
		return getUserInfo;
	}

	public Object userRegisteration(TimeStamp timeStamp) {

		String expDay = null;
		String expMonth = null;
		String expYear = null;
		String expireDate;

//
//		if (month.equals("A"))
//			month = "1";
//		else if (month.equals("B"))
//			month = "2";
//		else if (month.equals("C"))
//			month = "3";
//		else if (month.equals("D"))
//			month = "4";
//		else if (month.equals("E"))
//			month = "5";
//		else if (month.equals("F"))
//			month = "6";
//		else if (month.equals("G"))
//			month = "7";
//		else if (month.equals("H"))
//			month = "8";
//		else if (month.equals("I"))
//			month = "9";
//		else if (month.equals("J"))
//			month = "10";
//		else if (month.equals("K"))
//			month = "11";
//		else if (month.equals("L"))
//			month = "12";

		if (timeStamp.getDate().equals("")) {

			int exp_year = timeStamp.getRegiYear() + timeStamp.getYearsCount();

			int exp_month = timeStamp.getRegiMonth() + timeStamp.getMonthsCount();
			if (exp_month > 12) {
				exp_month -= 12;
				exp_year += 1;
			}

			int exp_day = timeStamp.getRegiDate() + timeStamp.getDaysCount();
			if (exp_day > 30) {
				exp_day -= 30;
				exp_month += 1;
				if (exp_month > 12)
					exp_year += exp_year;
			}

			expYear = String.valueOf(exp_year);

			if (exp_month < 10) {
				expMonth = "0" + String.valueOf(exp_month);
			} else {
				expMonth = String.valueOf(exp_month);
			}

			if (exp_day < 10) {
				expDay = "0" + String.valueOf(exp_day);
			} else {
				expDay = String.valueOf(exp_day);
			}

		} else if (!timeStamp.getDate().equals("")) {
			expDay = timeStamp.getDate().split("-")[2];
			expMonth = timeStamp.getDate().split("-")[1];
			expYear = timeStamp.getDate().split("-")[0].substring(2);

		}
		expireDate = expDay + "-" + expMonth + "-" + "20" + expYear;
		expMonth = getExpiryMonth(expMonth);

		String verificationKey = expMonth + "1" + timeStamp.getDeviceId().substring(0, 2) + "-"
				+ expYear.substring(0, 1) + timeStamp.getDeviceId().substring(2, 3) + expYear.substring(1)
				+ timeStamp.getDeviceId().substring(3, 4) + "-" + expDay.substring(0, 1)
				+ timeStamp.getDeviceId().substring(4, 5) + expDay.substring(1)
				+ timeStamp.getDeviceId().substring(5, 6) + "-" + timeStamp.getDistributerId()
				+ timeStamp.getDeviceId().substring(6);

//		jdbcTemplate.update(
//				"update register set appKey = ?, expiredate = ? where deviceId = '" + timeStamp.getDeviceId() + "'",
//				new Object[] { verificationKey, expireDate });

		return new Object[] { verificationKey = verificationKey, expireDate = expireDate };

	}

	public int setUserDetails(userPojo userInfo) {
		String regiDateFormated = "" + userInfo.getRegiDate().split("-")[2] + "-" + userInfo.getRegiDate().split("-")[1]
				+ "-" + userInfo.getRegiDate().split("-")[0] + "";
//		System.out.println("regiDateFormated:- " + regiDateFormated + "");

		List<userPojo> getuserInfo = jdbcTemplate.query(
				"select * from register where email = '" + userInfo.getEmail() + "' and distributerId = '"
						+ userInfo.getDistributerId() + "' and appId = '" + userInfo.getAppId() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		if (getuserInfo.size() == 0) {
			jdbcTemplate.update(
					"insert into register (firstName, lastName, contactNo, email, password, distributerId, appId, deviceId, appKey, regiDate, regiDateFormated, expireDate, status, activated, activationGivenBy) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new Object[] { userInfo.getFirstName(), userInfo.getLastName(), userInfo.getContactNo(),
							userInfo.getEmail(), userInfo.getPassword(), userInfo.getDistributerId(),
							userInfo.getAppId(), userInfo.getDeviceId(), userInfo.getAppKey(), userInfo.getRegiDate(),
							regiDateFormated, userInfo.getExpireDate(), "loggedIn", "true",
							userInfo.getActivationGivenBy() });
			deleteNotification(userInfo.getEmail(), userInfo.getDistributerId(), userInfo.getAppId());
		} else {
			activateAccount(userInfo);
		}
		return 1;
	}

	public void deleteNotification(String email, String distributerId, String appId) {
		System.out.println(" '" + email + "'," + distributerId + "'," + appId + "'");
		jdbcTemplate.update("delete from notiifications where email = ? and distributerId = ? and appId = ?",
				new Object[] { email, distributerId, appId });
	}

	public userPojo getUserDetails(userPojo userInfo) {
		List<userPojo> getUserInfo = jdbcTemplate.query(
				"select * from register where email = '" + userInfo.getEmail() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		return getUserInfo.get(0);
	}

	public userPojo getUserDetailsUpdated(userPojo userInfo) {
		List<userPojo> getUserInfo = jdbcTemplate.query(
				"select * from register where email = '" + userInfo.getEmail() + "' and distributerId = '"
						+ userInfo.getDistributerId() + "' and appId = '" + userInfo.getAppId() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		return getUserInfo.get(0);
	}

	public List<userPojo> getAllUsers() {
		List<userPojo> getUserInfo = jdbcTemplate.query("select * from register order by id desc",
				new BeanPropertyRowMapper(userPojo.class));
		return getUserInfo;
	}

	public List<userPojo> getAllUsersDistributerWise(String distributerId) {
		List<userPojo> getUserInfo = jdbcTemplate.query(
				"select * from register where distributerId = '" + distributerId + "' order by id desc",
				new BeanPropertyRowMapper(userPojo.class));
		return getUserInfo;
	}

	public userPojo searchByEmail(userPojo userInfo) {
		List<userPojo> getuserInfo = jdbcTemplate.query(
				"select * from register where email = '" + userInfo.getEmail() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		return getuserInfo.get(0);
	}

	public int fillUserInfo(userPojo userDetails) {
		System.out.println(userDetails.getBirthDate());
		jdbcTemplate.update(
				"update register set birthDate = ?, gender = ?, parentsContactNo = ?, standard = ?, state = ?, district = ?, taluka = ? where email = ?",
				new Object[] { userDetails.getBirthDate(), userDetails.getGender(), userDetails.getParentsContactNo(),
						userDetails.getStandard(), userDetails.getState(), userDetails.getDistrict(),
						userDetails.getTaluka(), userDetails.getEmail() });
		return 1;
	}

	public int fillUserInfoUpdated(userPojo userDetails) {
		System.out.println(userDetails.getBirthDate());
		jdbcTemplate.update(
				"update register set birthDate = ?, gender = ?, parentsContactNo = ?, standard = ?, state = ?, district = ?, taluka = ? where email = ? and distributerId = ? and appId = ?",
				new Object[] { userDetails.getBirthDate(), userDetails.getGender(), userDetails.getParentsContactNo(),
						userDetails.getStandard(), userDetails.getState(), userDetails.getDistrict(),
						userDetails.getTaluka(), userDetails.getEmail(), userDetails.getDistributerId(),
						userDetails.getAppId() });
		return 1;
	}

	// check user is register or not
	public int checkRegister(String Email) throws ClassNotFoundException, SQLException {
		List<userPojo> getData = jdbcTemplate.query("select * from register where email='" + Email + "'",
				new BeanPropertyRowMapper(userPojo.class));
		if (getData.isEmpty()) {
			return 1;
		} else {
			return 0;
		}
	}

	public int checkRegisterNew(String email, String distributerId, String appId)
			throws ClassNotFoundException, SQLException {
		List<userPojo> getData = jdbcTemplate.query("select * from register where email ='" + email
				+ "' and distributerId = '" + distributerId + "' and appId = '" + appId + "'",
				new BeanPropertyRowMapper(userPojo.class));
		if (getData.isEmpty()) {
			return 1;
		} else {
			return 0;
		}
	}

//to forgot password
	public int forgotPassword(userPojo userInfo) {
		jdbcTemplate.update("update register set password=? where email= ?",
				new Object[] { userInfo.getPassword(), userInfo.getEmail() });
		return 1;

	}

	public int forgotPasswordUpdated(userPojo userInfo) {
		System.out.println(userInfo.getAppId());
		List<userPojo> getData = jdbcTemplate.query(
				"select * from register where email='" + userInfo.getEmail() + "' and distributerId='"
						+ userInfo.getDistributerId() + "' and appId='" + userInfo.getAppId() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		if (getData.isEmpty()) {
			userInfo.setAppId("1");
			jdbcTemplate.update("update register set password=? where email= ? and distributerId = ? and appId = ?",
					new Object[] { userInfo.getPassword(), userInfo.getEmail(), userInfo.getDistributerId(),
							userInfo.getAppId() });
			return 1;
		} else {
			jdbcTemplate.update("update register set password=? where email= ? and distributerId = ? and appId = ?",
					new Object[] { userInfo.getPassword(), userInfo.getEmail(), userInfo.getDistributerId(),
							userInfo.getAppId() });
			return 1;

		}
	}

	// To login
	public userPojo login(userPojo userInfo) {
		List<userPojo> getData = jdbcTemplate.query("select * from register where email='" + userInfo.getEmail()
				+ "' and password='" + userInfo.getPassword() + "'", new BeanPropertyRowMapper(userPojo.class));
		return getData.get(0);
	}

	public userPojo loginUpdated(userPojo userInfo) {
		System.out.println("email = '" + userInfo.getEmail() + "'");
		List<userPojo> getData = jdbcTemplate.query("select * from register where email='" + userInfo.getEmail()
				+ "' and password='" + userInfo.getPassword() + "' and distributerId='" + userInfo.getDistributerId()
				+ "' and appId='" + userInfo.getAppId() + "'", new BeanPropertyRowMapper(userPojo.class));
		return getData.get(0);
	}

	public int setLoginStatus(userPojo userInfo) {
		jdbcTemplate.update("update register set status = 'loggedIn' where email = ?",
				new Object[] { userInfo.getEmail() });
		return 1;
	}

	public int setLoginStatusWithDeviceId(userPojo userInfo) {
		jdbcTemplate.update("update register set status = 'loggedIn', deviceId = ? where email = ?",
				new Object[] { userInfo.getDeviceId(), userInfo.getEmail() });
		return 1;
	}

	public int setLoginStatusWithDeviceIdUpdated(userPojo userInfo) {
		jdbcTemplate.update(
				"update register set status = 'loggedIn', deviceId = ? where email = ? and distributerId = ? and appId = ?",
				new Object[] { userInfo.getDeviceId(), userInfo.getEmail(), userInfo.getDistributerId(),
						userInfo.getAppId() });
		return 1;
	}

	public int logout(userPojo userInfo) {
		if (userInfo.getEmail().equals("")) {
			return 1;
		} else {
			jdbcTemplate.update("update register set status = 'loggedOut' where email = '" + userInfo.getEmail() + "'");
			return 1;
		}
	}

	public int logoutUpdated(userPojo userInfo) {
		if (userInfo.getEmail().equals("")) {
			return 1;
		} else {
			jdbcTemplate.update(
					"update register set status = 'loggedOut' where email =? and distributerId = ? and appId = ?",
					new Object[] { userInfo.getEmail(), userInfo.getDistributerId(), userInfo.getAppId() });
			return 1;
		}
	}

	// for test data
	public List<userPojo> getData() {

		List<userPojo> getData = jdbcTemplate.query("select * from register",
				new BeanPropertyRowMapper(userPojo.class));

		return getData;

	}

	public int setDemo(DemoPojo demo) {
		String dateFormated = "" + demo.getDate().split("-")[2] + "-" + demo.getDate().split("-")[1] + "-"
				+ demo.getDate().split("-")[0] + "";
		System.out.println("regiDateFormated:- " + dateFormated + "");
		jdbcTemplate.update(
				"insert into demo (firstName, lastName, contactNo, email, distributerId, appId, date, dateFormated, area) values (?,?,?,?,?,?,?,?,?)",
				new Object[] { demo.getFirstName(), demo.getLastName(), demo.getContactNo(), demo.getEmail(),
						demo.getDistributerId(), demo.getAppId(), demo.getDate(), dateFormated, demo.getArea() });
		return 1;
	}

	public int getDemo() {
		List<DemoPojo> getData = jdbcTemplate.query("select * from demo", new BeanPropertyRowMapper(DemoPojo.class));
		return getData.size();
	}

	public DemoPojo getDemoDetail() {
		List<DemoPojo> getData = jdbcTemplate.query("select * from demo order by id desc limit 1",
				new BeanPropertyRowMapper(DemoPojo.class));
		return getData.get(0);
	}

	public List<DemoPojo> getDemoDetails() {
		List<DemoPojo> getData = jdbcTemplate.query("select * from demo order by id desc",
				new BeanPropertyRowMapper(DemoPojo.class));
		return getData;
	}

	public List<DemoPojo> getDistributerWiseDemoDetails(String distributerId) {
		List<DemoPojo> getData = jdbcTemplate.query(
				"select * from demo where distributerId = '" + distributerId + "' order by id desc",
				new BeanPropertyRowMapper(DemoPojo.class));
		return getData;
	}

	public int deactivateAccount(userPojo userInfo) {
		jdbcTemplate.update(
				"update register set activated = 'false' where email = ? and distributerId = ? and appId = ?",
				new Object[] { userInfo.getEmail(), userInfo.getDistributerId(), userInfo.getAppId() });
		return 1;
	}

	public int activateAccount(userPojo userInfo) {
		jdbcTemplate.update(
				"update register set activated = 'true' where email = ? and distributerId = ? and appId = ?",
				new Object[] { userInfo.getEmail(), userInfo.getDistributerId(), userInfo.getAppId() });
		return 1;
	}

	public int getActivationDetails(userPojo userInfo) {
		List<userPojo> getData = jdbcTemplate.query(
				"select activated from register where email = '" + userInfo.getEmail() + "'",
				new BeanPropertyRowMapper(userPojo.class));
		if (getData.get(0).getActivated().equals("false")) {
			return 1;
		} else {
			return 0;
		}
	}

	public int getActivationDetailsUpdated(userPojo userInfo) {
		List<userPojo> getData = jdbcTemplate.query(
				"select activated from register where email = '" + userInfo.getEmail() + "'and distributerId='"
						+ userInfo.getDistributerId() + "' and appId='" + userInfo.getAppId() + "' ",
				new BeanPropertyRowMapper(userPojo.class));
		if (getData.get(0).getActivated().equals("false")) {
			return 1;
		} else {
			return 0;
		}
	}

	public Object getPaymentLink(int appId) {
		List<PaymentPojo> paymentLink = jdbcTemplate.query(
				"select paymentLink from paymentlinks where appId = '" + appId + "'",
				new BeanPropertyRowMapper(PaymentPojo.class));
		String link = paymentLink.get(0).getPaymentLink();
		return new Object[] { link = link };
	}

	public String getExpiryMonth(String expMonth) {
		if (expMonth.equals("01"))
			expMonth = "A";
		else if (expMonth.equals("02"))
			expMonth = "B";
		else if (expMonth.equals("03"))
			expMonth = "C";
		else if (expMonth.equals("04"))
			expMonth = "D";
		else if (expMonth.equals("05"))
			expMonth = "E";
		else if (expMonth.equals("06"))
			expMonth = "F";
		else if (expMonth.equals("07"))
			expMonth = "G";
		else if (expMonth.equals("08"))
			expMonth = "H";
		else if (expMonth.equals("09"))
			expMonth = "I";
		else if (expMonth.equals("10"))
			expMonth = "J";
		else if (expMonth.equals("11"))
			expMonth = "K";
		else if (expMonth.equals("12"))
			expMonth = "L";
		return expMonth;
	}

//	public List<RegiDate> getRegiDates() {
//		List<RegiDate> getRegiDates = jdbcTemplate.query("select distinct date from demo",
//				new BeanPropertyRowMapper(RegiDate.class));
//		return getRegiDates;
//	}
//
//	public int setRegiDates(RegiDate[] regiDate) {
//		for (int i = 0; i < regiDate.length; i++) {
//			jdbcTemplate.update("update demo set dateFormated = ? where date = ?",
//					new Object[] { regiDate[i].getDateFormated(), regiDate[i].getDate() });
//			System.out.println(i);
//		}
//
//		return 1;
//	}

	public List<userPojo> getBetweenTimeStamp(DateInBetween dateInBetween, String distributerId) {
		List<userPojo> getUsers = jdbcTemplate.query("select * from register where regiDateFormated between CONVERT('"
				+ dateInBetween.getFromDate() + "', DATE) AND CONVERT('" + dateInBetween.getToDate()
				+ "', DATE) AND distributerId='" + distributerId + "'", new BeanPropertyRowMapper(userPojo.class));
		return getUsers;
	}

	public List<DemoPojo> getBetweenTimeStampDemo(DateInBetween dateInBetween, String distributerId) {
		List<DemoPojo> getUsers = jdbcTemplate.query("select * from demo where dateFormated between CONVERT('"
				+ dateInBetween.getFromDate() + "', DATE) AND CONVERT('" + dateInBetween.getToDate()
				+ "', DATE) AND distributerId='" + distributerId + "'", new BeanPropertyRowMapper(DemoPojo.class));
		return getUsers;
	}

}

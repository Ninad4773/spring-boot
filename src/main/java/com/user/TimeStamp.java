package com.user;

public class TimeStamp {

	private String deviceId;
	private int daysCount;
	private int monthsCount;
	private int yearsCount;
	private int regiDate;
	private int regiMonth;
	private int regiYear;
	private String date;
	private String distributerId;
	private String appId;
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public int getDaysCount() {
		return daysCount;
	}
	public void setDaysCount(int daysCount) {
		this.daysCount = daysCount;
	}
	public int getMonthsCount() {
		return monthsCount;
	}
	public void setMonthsCount(int monthsCount) {
		this.monthsCount = monthsCount;
	}
	public int getYearsCount() {
		return yearsCount;
	}
	public void setYearsCount(int yearsCount) {
		this.yearsCount = yearsCount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDistributerId() {
		return distributerId;
	}
	public void setDistributerId(String distributerId) {
		this.distributerId = distributerId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public int getRegiDate() {
		return regiDate;
	}
	public void setRegiDate(int regiDate) {
		this.regiDate = regiDate;
	}
	public int getRegiMonth() {
		return regiMonth;
	}
	public void setRegiMonth(int regiMonth) {
		this.regiMonth = regiMonth;
	}
	public int getRegiYear() {
		return regiYear;
	}
	public void setRegiYear(int regiYear) {
		this.regiYear = regiYear;
	}
	
	
}

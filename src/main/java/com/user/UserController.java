package com.user;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import Admin.PaymentPojo;

@RestController
public class UserController {

	@Autowired
	UserRegister userRegister;

//@RequestMapping("/userRegister")
//@CrossOrigin
//  public String userRegister(@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,@RequestParam("contactNo") int contactNo,@RequestParam("Email") String Email,@RequestParam("Password") String Password,@RequestParam("Distributer") String Distributer,@RequestParam("appId") String appId,@RequestParam("deviceId") String deviceId,@RequestParam("appKey") String appKey,@RequestParam("regiDate") String regiDate,@RequestParam("expireDate") String expireDate,@RequestParam("Status") String Status) throws ClassNotFoundException, SQLException{
//	return userRegister.userRegister(firstName, lastName, contactNo, Email, Password, Distributer, appId, deviceId, appKey, regiDate, expireDate, Status);
//	 }

	@RequestMapping("/notify")
	@CrossOrigin
	public int setNotification(@RequestBody userPojo userInfo) {
		return userRegister.setNotification(userInfo);
	}

	@RequestMapping("/getNotificationDetails")
	@CrossOrigin
	public List<userPojo> getNotificationinfo() {
		return userRegister.getNotificationDetails();
	}

	@RequestMapping("/userRegister")
	@CrossOrigin
	public Object userRegister(@RequestBody TimeStamp timeStamp) {
		return userRegister.userRegisteration(timeStamp);
	}

	@RequestMapping("/setUserInfo")
	@CrossOrigin
	public int setUserinfo(@RequestBody userPojo userInfo) {
		return userRegister.setUserDetails(userInfo);
	}

	@RequestMapping("/getUserInfo")
	@CrossOrigin
	public userPojo getUserinfo(@RequestBody userPojo userInfo) {
		return userRegister.getUserDetails(userInfo);
	}

	@RequestMapping("/getUserInfoUpdated")
	@CrossOrigin
	public userPojo getUserinfoUpdated(@RequestBody userPojo userInfo) {
		return userRegister.getUserDetailsUpdated(userInfo);
	}

	@RequestMapping("/getAllUsers")
	@CrossOrigin
	public List<userPojo> getAllUsers() {
		return userRegister.getAllUsers();
	}

	@RequestMapping("/getAllUsersDistributerWise")
	@CrossOrigin
	public List<userPojo> getAllUsersDistributerWise(@RequestParam("distributerId") String distributerId) {
		return userRegister.getAllUsersDistributerWise(distributerId);
	}

	@RequestMapping("/fillUserDetails")
	@CrossOrigin
	public int fillUserInfo(@RequestBody userPojo userDetails) {
		return userRegister.fillUserInfo(userDetails);
	}

	@RequestMapping("/fillUserDetailsUpdated")
	@CrossOrigin
	public int fillUserInfoUpdated(@RequestBody userPojo userDetails) {
		return userRegister.fillUserInfoUpdated(userDetails);
	}

	@RequestMapping("/searchByEmail")
	@CrossOrigin
	public userPojo searchByEmail(@RequestBody userPojo userDetails) {
		return userRegister.searchByEmail(userDetails);
	}

	@RequestMapping("/checkRegister")
	@CrossOrigin
	public int checkRegister(@RequestBody userPojo userDetails) throws ClassNotFoundException, SQLException {
		return userRegister.checkRegister(userDetails.getEmail());
	}

	@RequestMapping("/checkRegisterWithDeviceIdAndAppID")
	@CrossOrigin
	public int checkRegisterNew(@RequestBody userPojo userDetails) throws ClassNotFoundException, SQLException {
		return userRegister.checkRegisterNew(userDetails.getEmail(), userDetails.getDistributerId(),
				userDetails.getAppId());
	}

	@RequestMapping("/forgotPassword")
	@CrossOrigin
	public int forgotPassword(@RequestBody userPojo userInfo) {
		return userRegister.forgotPassword(userInfo);
	}

	@RequestMapping("/forgotPasswordUpdated")
	@CrossOrigin
	public int forgotPasswordUpdated(@RequestBody userPojo userInfo) {
		return userRegister.forgotPasswordUpdated(userInfo);
	}

	@RequestMapping("/login")
	@CrossOrigin
	public userPojo login(@RequestBody userPojo userInfo) {
		return userRegister.login(userInfo);
	}

	@RequestMapping("/loginUpdated")
	@CrossOrigin
	public userPojo loginUpdated(@RequestBody userPojo userInfo) {
		return userRegister.loginUpdated(userInfo);
	}

	@RequestMapping("/setLoginStatus")
	@CrossOrigin
	public int setLoginStatus(@RequestBody userPojo userInfo) {
		return userRegister.setLoginStatus(userInfo);
	}

	@RequestMapping("/setLoginStatusWithDeviceId")
	@CrossOrigin
	public int setLoginStatusWithDeviceId(@RequestBody userPojo userInfo) {
		return userRegister.setLoginStatusWithDeviceId(userInfo);
	}

	@RequestMapping("/setLoginStatusWithDeviceIdUpdated")
	@CrossOrigin
	public int setLoginStatusWithDeviceIdUpdated(@RequestBody userPojo userInfo) {
		return userRegister.setLoginStatusWithDeviceIdUpdated(userInfo);
	}

	@RequestMapping("/deactivateAccount")
	@CrossOrigin
	public int deactivateAccount(@RequestBody userPojo userInfo) {
		return userRegister.deactivateAccount(userInfo);
	}

	@RequestMapping("/activateAccount")
	@CrossOrigin
	public int activateAccount(@RequestBody userPojo userInfo) {
		return userRegister.activateAccount(userInfo);
	}

	@RequestMapping("/getActivationDetails")
	@CrossOrigin
	public int getActivationDetails(@RequestBody userPojo userInfo) {
		return userRegister.getActivationDetails(userInfo);
	}

	@RequestMapping("/getActivationDetailsUpdated")
	@CrossOrigin
	public int getActivationDetailsUpdated(@RequestBody userPojo userInfo) {
		return userRegister.getActivationDetailsUpdated(userInfo);
	}

	@RequestMapping("/logout")
	@CrossOrigin
	public int logout(@RequestBody userPojo userInfo) {
		return userRegister.logout(userInfo);
	}

	@RequestMapping("/logoutUpdated")
	@CrossOrigin
	public int logoutUpdated(@RequestBody userPojo userInfo) {
		return userRegister.logoutUpdated(userInfo);
	}

	@RequestMapping("/setDemo")
	@CrossOrigin
	public int setDemo(@RequestBody DemoPojo demo) {
		return userRegister.setDemo(demo);
	}

	@RequestMapping("/getDemo")
	@CrossOrigin
	public int getDemo() {
		return userRegister.getDemo();
	}

	@RequestMapping("/getDemoDetail")
	@CrossOrigin
	public DemoPojo getDemoDetail() {
		return userRegister.getDemoDetail();
	}

	@RequestMapping("/getDemoDetails")
	@CrossOrigin
	public List<DemoPojo> getDemoDetails() {
		return userRegister.getDemoDetails();
	}

	@RequestMapping("/getDistributerWiseDemoDetails")
	@CrossOrigin
	public List<DemoPojo> getDistributerWiseDemoDetails(@RequestParam("distributerId") String distributerId) {
		return userRegister.getDistributerWiseDemoDetails(distributerId);
	}

//getData for test
	@RequestMapping("/getData")
	@CrossOrigin
	public List<userPojo> getData() {
		return userRegister.getData();
	}

	@RequestMapping("/getPaymentLink")
	@CrossOrigin
	public Object getPaymentLink(@RequestBody PaymentPojo payment) {
		return userRegister.getPaymentLink(payment.getAppId());
	}
	
//	@RequestMapping("/getRegiDates")
//	@CrossOrigin	
//	public List<RegiDate> getRegiDates() {
//		return userRegister.getRegiDates();
//	}
////	
//	@RequestMapping("/setRegiDates")
//	@CrossOrigin	
//	public int setRegiDates(@RequestBody RegiDate[] regiDate) {
//		return userRegister.setRegiDates(regiDate);
//	}
//
	@RequestMapping("/getBetweenTimeStamp")
	@CrossOrigin
	public List<userPojo> getBetweenTimeStamp(@RequestBody DateInBetween dateInBetween, @RequestParam("distributerId") String distributerId) {
		return userRegister.getBetweenTimeStamp(dateInBetween, distributerId);
	}
	
	@RequestMapping("/getBetweenTimeStampDemo")
	@CrossOrigin
	public List<DemoPojo> getBetweenTimeStampDemo(@RequestBody DateInBetween dateInBetween, @RequestParam("distributerId") String distributerId) {
		return userRegister.getBetweenTimeStampDemo(dateInBetween, distributerId);
	}
}
